package com.antoniorico.buscadorproductos

interface BasePresenter {

    fun start()

}