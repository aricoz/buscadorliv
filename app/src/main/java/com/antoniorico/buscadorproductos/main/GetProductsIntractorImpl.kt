package com.antoniorico.buscadorproductos.main

import android.util.Log
import com.antoniorico.buscadorproductos.main.MainContract.GetProductIntractor.OnFinishedListener
import com.antoniorico.buscadorproductos.model.ProductListResponse
import com.antoniorico.buscadorproductos.network.GetProductsDataService
import com.antoniorico.buscadorproductos.network.RetrofitInstance
import com.antoniorico.buscadorproductos.utils.Constants
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetProductsIntractorImpl : MainContract.GetProductIntractor {
    override fun getProductArrayListBySearch(onFinishedListener: OnFinishedListener, search: String?, pageToAdd: Int) {
        val service = RetrofitInstance.getRetrofitInstance().create(GetProductsDataService::class.java)

        val realSearch = if (search.isNullOrEmpty() || search.isNullOrBlank()) "a" else search
        val call = service.productDataByPage(Constants.forcePlp, realSearch, pageToAdd, Constants.itemsPerPage)

        Log.wtf("URL Called", call?.request()?.url().toString())
        call?.enqueue(object : Callback<ProductListResponse?> {
            override fun onFailure(call: Call<ProductListResponse?>, t: Throwable) {
                onFinishedListener.onFailure(t)
            }

            override fun onResponse(call: Call<ProductListResponse?>, response: Response<ProductListResponse?>) {
                if (response.code() == 200) {
                    onFinishedListener.onFinished(response.body()!!)
                }  else if (response.code() == 404)
                    onFinishedListener.onFailure(null)
            }
        })
    }
}