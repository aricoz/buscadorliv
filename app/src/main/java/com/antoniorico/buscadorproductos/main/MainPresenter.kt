package com.antoniorico.buscadorproductos.main

import com.antoniorico.buscadorproductos.model.ProductListResponse


class MainPresenter(
    private var mainView: MainContract.View?,
    private val getProductIntractor: MainContract.GetProductIntractor
) : MainContract.Presenter, MainContract.GetProductIntractor.OnFinishedListener {
    override fun start() {
        //Do nothing
    }

    override fun searchProduct(search: String, pageToAdd: Int) {

        getProductIntractor.getProductArrayListBySearch(this, search, pageToAdd)
    }

    override fun onDestroy() {
        mainView = null
    }

    override fun onRefreshButtonClick() {
        if (mainView != null) {
            mainView!!.showProgress()
        }
        getProductIntractor.getProductArrayListBySearch(this, "",1)
    }

    override fun requestDataFromServer() {
        getProductIntractor.getProductArrayListBySearch(this, "",1)
    }

    override fun onFinished(productArrayList: ProductListResponse) {
        if (mainView != null) {
            mainView!!.setDataToRecyclerView(productArrayList)
            mainView!!.hideProgress()
        }
    }

    override fun onFailure(t: Throwable?) {
        if (mainView != null) {
            mainView!!.onResponseFailure(t)
            mainView!!.hideProgress()
        }
    }
}