package com.antoniorico.buscadorproductos.main

import com.antoniorico.buscadorproductos.BasePresenter
import com.antoniorico.buscadorproductos.BaseView
import com.antoniorico.buscadorproductos.model.ProductListResponse

interface MainContract {
    interface View : BaseView<Presenter> {
        fun showProgress()

        fun hideProgress()

        fun setDataToRecyclerView(productArrayList: ProductListResponse)

        fun onResponseFailure(throwable: Throwable?)
    }

    interface Presenter : BasePresenter {
        fun onDestroy()

        fun onRefreshButtonClick()

        fun requestDataFromServer()

        fun searchProduct(search: String, pageToAdd: Int)
    }

    interface GetProductIntractor {
        interface OnFinishedListener {
            fun onFinished(productArrayList: ProductListResponse)
            fun onFailure(t: Throwable?)
        }

        fun getProductArrayListBySearch(onFinishedListener: OnFinishedListener, search: String?, pageToAdd: Int)
    }
}
