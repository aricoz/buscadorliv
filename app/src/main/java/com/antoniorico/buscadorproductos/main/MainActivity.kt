package com.antoniorico.buscadorproductos.main

import android.app.ActionBar
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.antoniorico.buscadorproductos.BaseActivity
import com.antoniorico.buscadorproductos.R
import com.antoniorico.buscadorproductos.adapter.ProductAdapter
import com.antoniorico.buscadorproductos.model.ProductListResponse
import com.antoniorico.buscadorproductos.utils.Utilities
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity(), MainContract.View, ProductAdapter.OnClickListener{
    override lateinit var presenter: MainContract.Presenter
    private var productList: ArrayList<ProductListResponse.Result>?= ArrayList()
    private var actualPage = 0
    private var adapterProducts: ProductAdapter?=null
    private lateinit var linearManager: LinearLayoutManager
    private lateinit var mActivity: Activity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mActivity = this
        initializeToolbarAndRecyclerView()

        rootView.requestFocus()

        presenter = MainPresenter(this, GetProductsIntractorImpl())



        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(this, android.R.layout.select_dialog_item, Utilities.getSearchs(mActivity))
        eTSearch.threshold = 1 //will start working from first character
        eTSearch.setAdapter(adapter) //setting the adapter data into the AutoCompleteTextView
        eTSearch.setTextColor(Color.BLACK)
        searchBtn.setOnClickListener {
            if (eTSearch.text.toString().isNotBlank()) {
                nestedProducts.scrollTo(0, 0)
                productList?.removeAll(productList!!)
                actualPage = 0
                loadNextPage()
                Utilities.addSearch(eTSearch.text.toString(), mActivity)
            }
        }

        nestedProducts.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, _ ->
            rootView.requestFocus()
            hideKeyboard()
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight && progress.visibility == View.VISIBLE) {
                loadNextPage()
            }
        })

        loadNextPage()
    }

    private fun initializeToolbarAndRecyclerView() {
        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setCustomView(R.layout.action_bar_custom)
        supportActionBar!!.customView.findViewById<TextView>(R.id.tvTitle).text = resources.getString(R.string.txt_product)
    }

    override fun showProgress() {
        //Do nothing
    }

    override fun hideProgress() {
        //Do nothing
    }

    override fun setDataToRecyclerView(productArrayList: ProductListResponse) {
        if (actualPage == 1) {
            productList = productArrayList.plp?.results
            adapterProducts = ProductAdapter(productList!!, this, this)
            linearManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            recyclerProducts.layoutManager = linearManager
            recyclerProducts.adapter = adapterProducts
        } else {
            val actualSize = productList?.size!!
            productList?.addAll(productArrayList.plp?.results!!)
            adapterProducts?.notifyItemRangeChanged(actualSize, productList?.size!! - 1)
        }
    }

    override fun onResponseFailure(throwable: Throwable?) {
        Toast.makeText(this, resources.getString(R.string.error_generic), Toast.LENGTH_SHORT).show()
        progress.visibility = View.GONE
    }

    override fun onClickProduct(product: ProductListResponse.Result) {
        val uris = Uri.parse(product.lgImage)
        val intents = Intent(Intent.ACTION_VIEW, uris)
        val b = Bundle()
        b.putBoolean("new_window", true)
        intents.putExtras(b)
        startActivity(intents)
    }

    private fun loadNextPage() {
        if (isNetworkAvailable()){
            progress.visibility = View.VISIBLE
            if (eTSearch.text.toString().isNotEmpty() && eTSearch.text.toString().isNotBlank()) {
                actualPage++
                presenter.searchProduct(eTSearch.text.toString(), actualPage)
            } else {
                actualPage++
                presenter.searchProduct("", actualPage)
            }
        } else {
            Toast.makeText(this, resources.getString(R.string.error_network), Toast.LENGTH_SHORT).show()
            progress.visibility = View.GONE
        }
    }
}
