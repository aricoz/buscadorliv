package com.antoniorico.buscadorproductos.model

import com.google.gson.annotations.SerializedName

import java.util.ArrayList

class ProductListResponse {
    @SerializedName("plpResults")
    internal var plp: Plp? = null

    @SerializedName("status")
    internal var status: Status? = null

    class Status {
        @SerializedName("status")
        internal var status: String? = null

        @SerializedName("statusCode")
        internal var statusCode: String? = null
    }

    class Plp {
        @SerializedName("records")
        internal var results: ArrayList<Result>? = null
    }

    class Result {
        @SerializedName("productId")
        internal var productId: String? = null

        @SerializedName("productDisplayName")
        internal var productDisplayName: String? = null

        @SerializedName("productType")
        internal var productType: String? = null

        @SerializedName("productRatingCount")
        internal var productRatingCount: String? = null

        @SerializedName("productAvgRating")
        internal var productAvgRating: String? = null

        @SerializedName("listPrice")
        internal var listPrice: String? = null

        @SerializedName("seller")
        internal var seller: String? = null

        @SerializedName("category")
        internal var category: String? = null

        @SerializedName("smImage")
        internal var smImage: String? = null

        @SerializedName("lgImage")
        internal var lgImage: String? = null

        @SerializedName("xlImage")
        internal var xlImage: String? = null

        @SerializedName("variantsColor")
        internal var variantsColor: ArrayList<VariantsColor>? = null

        class VariantsColor{
            @SerializedName("colorName")
            internal var colorName: String? = null

            @SerializedName("colorHex")
            internal var colorHex: String? = null

            @SerializedName("colorImageURL")
            internal var colorImageURL: String? = null
        }
    }
}
