package com.antoniorico.buscadorproductos.utils

object Constants {
    const val BASE_URL =
        "https://shoppapp.liverpool.com.mx/appclienteservices/services/v3/"
    const val forcePlp = true
    const val itemsPerPage = 20
}