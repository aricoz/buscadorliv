package com.antoniorico.buscadorproductos.utils

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import java.io.IOException
import java.text.NumberFormat


object Utilities {
    const val SHARED_PREFS = "com.antoniorico.buscadorproductos.utils.SHARED_PREFS"
    const val TASKS = "com.antoniorico.buscadorproductos.utils.TASKS"
    private lateinit var currentSearchs: ArrayList<String?>

    fun formatCurrency(value: String): String {
        val format = NumberFormat.getCurrencyInstance()
        return format.format(value.toFloat().toDouble())
    }

    fun addSearch(search: String?, mActivity: Activity) {
        if (null == currentSearchs) {
            currentSearchs = ArrayList<String?>()
        }
        for (actualSearch: String? in currentSearchs) {
            if (actualSearch == search) {
                return
            }
        }

        currentSearchs.add(search)

        val prefs: SharedPreferences = mActivity.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = prefs.edit()
        try {
            editor.putString(TASKS, ObjectSerializer.serialize(currentSearchs))
        } catch (e: IOException) {
            Log.e("", e.toString())
        }
        editor.commit()
    }

    fun getSearchs(mActivity: Activity): ArrayList<String?> {
        // load tasks from preference
        // load tasks from preference
        val prefs: SharedPreferences = mActivity.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE)

        try {
            currentSearchs = ObjectSerializer.deserialize(
                prefs.getString(
                    TASKS,
                    ObjectSerializer.serialize(ArrayList<String?>())
                )
            ) as ArrayList<String?>
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } finally {
            return currentSearchs
        }
    }
}