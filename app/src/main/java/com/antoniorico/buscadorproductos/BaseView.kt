package com.antoniorico.buscadorproductos

interface BaseView<T> {

    var presenter: T
}