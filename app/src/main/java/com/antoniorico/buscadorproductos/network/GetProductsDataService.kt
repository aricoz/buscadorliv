package com.antoniorico.buscadorproductos.network


import com.antoniorico.buscadorproductos.model.ProductListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GetProductsDataService {
    @GET("plp")
    fun productDataByPage(@Query("force-plp") forcePlp: Boolean,
                            @Query("search-string") search: String,
                            @Query("page-number") page: Int,
                            @Query("number-of-items-per-page") itemsPerPage: Int
    ): Call<ProductListResponse?>?
}