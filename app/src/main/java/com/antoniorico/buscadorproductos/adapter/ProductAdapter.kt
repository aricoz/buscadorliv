package com.antoniorico.buscadorproductos.adapter

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.antoniorico.buscadorproductos.R
import com.antoniorico.buscadorproductos.model.ProductListResponse
import com.antoniorico.buscadorproductos.utils.Utilities
import com.bumptech.glide.Glide

class ProductAdapter(private val products: List<ProductListResponse.Result>, private val listener: OnClickListener, private val mContext: Activity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val RESULT = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        var holder: RecyclerView.ViewHolder? = null

        when (viewType) {
            RESULT -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.card_product, parent, false)
                holder = ViewHolderProduct(view)
            }
        }
        return holder!!
    }

    override fun getItemViewType(position: Int): Int {
        when (products[position]) {
            is ProductListResponse.Result -> return RESULT
            else -> return -1
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            RESULT -> {
                val holderH = holder as ViewHolderProduct
                val product  = products[position]

                holderH.titulo.text = product.productDisplayName
                holderH.price.text = Utilities.formatCurrency(product.listPrice?:"0.0")
                holderH.rootView.setOnClickListener {
                    listener.onClickProduct(product)
                }

                Glide
                    .with(mContext)
                    .load(product.lgImage)
                    .into(holderH.productImg)
                    .onLoadFailed(mContext.getDrawable(R.drawable.liv))

                holderH.rateText.text = product.productAvgRating?: "0.0"
                holderH.type.text = product.category?:""
            }
        }
    }

    override fun getItemCount(): Int = products.size

    class ViewHolderProduct(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rootView: LinearLayout = itemView.findViewById(R.id.rootView)
        var productImg: ImageView = itemView.findViewById(R.id.productImg)
        var titulo: TextView = itemView.findViewById(R.id.titulo)
        var price: TextView = itemView.findViewById(R.id.price)
        var type: TextView = itemView.findViewById(R.id.type)
        var rateText: TextView = itemView.findViewById(R.id.rateText)
    }

    interface OnClickListener {
        fun onClickProduct(product: ProductListResponse.Result)
    }
}